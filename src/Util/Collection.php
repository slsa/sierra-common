<?php

namespace Sierra\Common\Util;

use ArrayAccess;
use Countable;
use IteratorAggregate;
use JsonSerializable;
use Serializable;

/**
 * A general purpose collection class
 */
class Collection implements ArrayAccess, Countable, IteratorAggregate, JsonSerializable, Serializable
{
    /**
     * @var array
     */
    protected $data;

    /**
     * Class constructor.
     * Optionally initialise the collection with an array or \stdClass instance
     * @param mixed $data An array or object to initialise the collection with
     */
    public function __construct($data = [])
    {
        $this->replace($data);
    }

    /**
     * Property overloading implementation
     * @inheritdoc
     */
    public function __get($name)
    {
        return $this->offsetGet($name);
    }

    /**
     * Property overloading implementation
     * @inheritdoc
     */
    public function __isset($name)
    {
        return $this->offsetExists($name);
    }

    /**
     * Property overloading implementation
     * @inheritdoc
     */
    public function __set($name, $value)
    {
        $this->offsetSet($name, $value);
    }

    /**
     * Property overloading implementation
     * @inheritdoc
     */
    public function __unset($name)
    {
        $this->offsetUnset($name);
    }

    /**
     * Countable interface implementation
     * @inheritDoc
     */
    public function count()
    {
        return count($this->data);
    }

    /**
     * IteratorAggregate interface implementation
     * @inheritDoc
     */
    public function getIterator()
    {
        return new \ArrayIterator($this->data);
    }

    /**
     * JsonSerializable interface implementation
     * @inheritdoc
     */
    public function jsonSerialize()
    {
        return $this->data;
    }

    /**
     * ArrayAccess interface implementation
     * @inheritDoc
     */
    public function offsetExists($offset)
    {
        return array_key_exists($offset, $this->data);
    }

    /**
     * ArrayAccess interface implementation
     * @inheritDoc
     */
    public function offsetGet($offset)
    {
        if ($this->offsetExists($offset)) {
            return $this->data[$offset];
        }
    }

    /**
     * ArrayAccess interface implementation
     * @inheritDoc
     */
    public function offsetSet($offset, $value)
    {
        $this->data[$offset] = $value;
    }

    /**
     * ArrayAccess interface implementation
     * @inheritDoc
     */
    public function offsetUnset($offset)
    {
        unset($this->data[$offset]);
    }

    /**
     * Replace the collection contents by passing in an array or object
     * @param mixed $data The collection contents as an array or object
     */
    public function replace($data)
    {
        if ($data instanceof \stdClass) {
            $data = (array) $data;
        }

        if (!is_array($data)) {
            throw new \InvalidArgumentException('Expected data must be an array or an instance of stdClass');
        }

        $this->data = $data;
    }

    /**
     * Serializable interface implementation
     * @inheritDoc
     */
    public function serialize()
    {
        return serialize($this->data);
    }

    /**
     * Serializable interface implementation
     * @inheritDoc
     */
    public function unserialize($serialized)
    {
        $this->data = unserialize($serialized);
    }
}
