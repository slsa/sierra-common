<?php

use PHPUnit\Framework\TestCase;

class CollectionTest extends TestCase
{
    protected $bag;
    protected $property;

    public function setUp()
    {
        $this->bag = new \Sierra\Common\Util\Collection();
        $this->property = new \ReflectionProperty($this->bag, 'data');
        $this->property->setAccessible(true);
    }

    public function testReplace()
    {
        $data = [
            'abc' => '123',
            'foo' => 'bar'
        ];
        $this->bag->replace($data);

        $this->assertArrayHasKey('abc', $this->property->getValue($this->bag));
        $this->assertArrayHasKey('foo', $this->property->getValue($this->bag));

        $bag = $this->property->getValue($this->bag);
        $this->assertEquals($data['abc'], $bag['abc']);
        $this->assertEquals($data['foo'], $bag['foo']);
    }

    public function testArrayAccessGet()
    {
        $data = [
            'abc' => '123',
            'foo' => 'bar'
        ];
        $this->bag->replace($data);

        $this->assertEquals($data['foo'], $this->bag['foo']);
    }

    public function testArrayAccessSet()
    {
        $data = [
            'abc' => '123',
            'foo' => 'bar'
        ];
        $this->bag->replace($data);

        $newValue = 'updated';
        $this->bag['foo'] = $newValue;
        $bag = $this->property->getValue($this->bag);
        $this->assertEquals($newValue, $bag['foo']);
    }

    public function testArrayAccessExists()
    {
        $data = [
            'abc' => '123',
            'foo' => 'bar'
        ];
        $this->bag->replace($data);

        $this->assertTrue(isset($this->bag['foo']));
        $this->assertFalse(isset($this->bag['bar']));
    }

    public function testArrayAccessUnset()
    {
        $data = [
            'abc' => '123',
            'foo' => 'bar'
        ];
        $this->bag->replace($data);

        unset($this->bag['foo']);
        unset($data['foo']);
        $this->assertEquals($data, $this->property->getValue($this->bag));
    }

    public function testCount()
    {
        $data = [
            'abc' => '123',
            'foo' => 'bar'
        ];
        $this->bag->replace($data);

        $this->assertEquals(2, count($this->bag));
    }

    public function testGetIterator()
    {
        $data = [
            'abc' => '123',
            'foo' => 'bar'
        ];
        $this->bag->replace($data);

        $this->assertInstanceOf('\ArrayIterator', $this->bag->getIterator());
    }

    public function testPropertyOverloadGet()
    {
        $data = [
            'abc' => '123',
            'foo' => 'bar'
        ];
        $this->bag->replace($data);

        $this->assertEquals($data['abc'], $this->bag->abc);
        $this->assertEquals($data['foo'], $this->bag->foo);
    }

    public function testPropertyOverloadSet()
    {
        $data = [];
        $this->bag->replace($data);

        $this->bag->foobar = true;
        $this->assertArrayHasKey('foobar', $this->property->getValue($this->bag));
        $this->assertEquals(true, $this->bag->foobar);
    }

    public function testPropertyOverloadingIsset()
    {
        $data = [
            'abc' => '123',
            'foo' => 'bar'
        ];
        $this->bag->replace($data);

        $this->assertTrue(isset($this->bag->abc));
        $this->assertTrue(isset($this->bag->foo));
        $this->assertFalse(isset($this->bag->foobar));
    }

    public function testPropertyOverloadingUnset()
    {
        $data = [
            'abc' => '123',
            'foo' => 'bar'
        ];
        $this->bag->replace($data);

        $this->assertTrue(isset($this->bag->abc));
        unset($this->bag->abc);

        $this->assertFalse(isset($this->bag->abc));
        $this->assertArrayNotHasKey('abc', $this->property->getValue($this->bag));
        $this->assertArrayHasKey('foo', $this->property->getValue($this->bag));
    }
}
